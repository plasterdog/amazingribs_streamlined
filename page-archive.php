
<?php 
/**
 * Template Name: Archive Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package amazingribs_reworked
 */

get_header();

?>

<?php
if ( have_posts() ) : while ( have_posts() ): the_post(); ?>

    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

      <h1><?php the_title(); ?></h1>

<!-- PUBLISHED INFO -->   
<p class="last-modified">published on: <?php the_time('l, F j, Y'); ?>   </p>
<!-- https://pagely.com/blog/display-post-last-updated-wordpress/ -->
<?php
function show_last_modified_date( $content ) {
$original_time = get_the_time('U');
$modified_time = get_the_modified_time('U');
if ($modified_time >= $original_time + 86400) {
$updated_time = get_the_modified_time('h:i a');
$updated_day = get_the_modified_time('l, F j, Y');
$modified_content .= '<p class="last-modified">updated on '. $updated_day . '</p>';
}
$modified_content .= $content;
return $modified_content;
}
add_filter( 'the_content', 'show_last_modified_date' );
?>
<!-- ENDS PUBLISHED INFO -->

        <div class="post-excerpt">
          <?php the_content(); ?>
              <?php
      wp_link_pages( array(
        'before' => '<div class="page-links">' . __( 'Pages:', 'amazingribs_reworked' ),
        'after'  => '</div>',
      ) );
    ?>
        </div><!-- ends post excerpt container div -->

    </div><!-- ends the post content container -->

<?php endwhile;
endif;
?>
<!-- https://wpza.net/how-to-paginate-a-custom-post-type-in-wordpress/ -->
<?php if ( get_field( 'category_slug_name' ) ): ?>
<?php
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$args = array(
     'post_type' => 'post',
     'posts_per_page' => 10,
     'category_name'   => get_field('category_slug_name'), 
     'post_status'             => array( 'publish' ),
     'paged' => $paged
);
?>
<?php else: // field_name returned false ?>
<?php
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$args = array(
     'post_type' => 'post',
     'posts_per_page' => 10,
     
     'post_status'             => array( 'publish' ),
     'paged' => $paged
);
?>

<?php endif; // end of if field_name logic ?>

<?php
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>

    <div id="post-<?php the_ID(); ?>" class="post-item">
       
      <?php if ( get_field( 'alternate_featured_image_over_ride' ) ): ?>
            <a href="<?php the_permalink(); ?>" rel="bookmark">
        <img style="width:150px;" src="<?php the_field('alternate_featured_image_over_ride'); ?>"/></a>
      <?php else: // field_name returned false ?>
      <?php if ( has_post_thumbnail() ) { ?>
        <a href="<?php the_permalink(); ?>" rel="bookmark">
        <?php the_post_thumbnail( 'thumbnail' ); ?></a>
      <?php } else { ?>
        <a href="<?php the_permalink(); ?>" rel="bookmark">
        <img style="width:150px;" src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/default-thumb.jpg" alt="<?php the_title(); ?>" /></a>
        <?php } ?>  
      <?php endif; // end of if field_name logic ?>


       <div class="content">
            <?php if ( get_field( 'alternate_title_over_ride' ) ): ?>
            <h2><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_field('alternate_title_over_ride'); ?></a></h2>
        <?php else: // field_name returned false ?>
            <h2><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
        <?php endif; // end of if field_name logic ?>

            <div class="post-excerpt"><?php the_excerpt(); ?></div>
        </div>
    </div>

<?php endwhile;
?>
<nav class="pagination">
     <?php
     $big = 999999999;
     echo paginate_links( array(
          'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
          'format' => '?paged=%#%',
          'current' => max( 1, get_query_var('paged') ),
          'total' => $loop->max_num_pages,
          'prev_text' => '&laquo; Previous',
          'next_text' => 'Next &raquo;'
     ) );
?>
</nav>


    <nav class="pagination">
        <?php pagination_bar(); ?>
    </nav>
<!-- ALL OF THIS USED TO BE IN THE FOOTER -->
</div><!-- /.row -->


<section class="comments">
   <?php if ( is_active_sidebar( 'upper-footer' ) ) : ?>
          <?php dynamic_sidebar( 'upper-footer' ); ?> 
<?php endif; ?>
</section>

</div><!-- ends container -->


<!-- CONDITION FOR ALTERNATE SIDEBAR -->
<div class="widget-section widget-left">
    
                  <?php if ( ! dynamic_sidebar( 'left-page' ) ) : ?>
                  <?php endif; // end sidebar widget area ?>
        
</div>

<!-- ENDS CONDITION FOR ALTERNATE SIDEBAR -->


    <?php if ( is_active_sidebar( 'right-page' ) ) : ?>
      <div class="widget-section widget-right">
        <?php dynamic_sidebar( 'right-page' ); ?>
      </div>
    <?php endif; ?>


</div><!-- row? -->
</div><!-- container? -->
<!-- NOW GET THE MODIFIED FOOTER -->
<?php get_footer(); ?>