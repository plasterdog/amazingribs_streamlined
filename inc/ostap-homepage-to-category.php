<?php
// -OSTAP HOMEPAGE TO CATEGORY
function replace_homepage_to_category( $query ) {
    // Only modify the main query
    // on the blog posts index page
    if ( is_home() && $query->is_main_query() && is_numeric(get_option('homepage-category', true)) && get_option('homepage-category', true) > 0) {
        $query->set( 'cat', get_option('homepage-category', true) );
    }

}
add_action( 'pre_get_posts', 'replace_homepage_to_category' );

add_filter( 'template_include', 'homepage_page_category_template', 99 );

function homepage_page_category_template( $template )
{
    global $wp_query;
    if (is_home() && $wp_query->get( 'cat' )) {
        $new_template = locate_template(array('category.php'));
        if ('' != $new_template) {
            return $new_template;
        }
    }
    return $template;
}



/* Admin init */
add_action( 'admin_init', 'my_settings_init' );

/* Settings Init */
function my_settings_init(){

    /* Register Settings */
    register_setting(
        'writing',             // Options group
        'homepage-category',      // Option name/database
        '' // Sanitize callback function
    );

    /* Create settings field */
    add_settings_field(
        'homepage-category-setting',       // Field ID
        'Default Homepage Category',       // Field title
        'list_all_categories_setting', // Field callback function
        'writing',                    // Settings page slug
        'default'               // Section ID
    );

}
/* Settings Field Callback */
function list_all_categories_setting(){
    ?>
    <select id="homepage-category" name="homepage-category" >
        <option value="0">-- None --</option>
        <?php
        $categories = get_categories();
        foreach ($categories as $category) {
            $selected = "";
            if(get_option('homepage-category', true) == $category->term_id) $selected="selected";
            echo "<option $selected value='".$category->term_id."'>".$category->name."</option>";
        }
        ?>
    </select>
    <?php
}
// - ENDS OSTAP HOMEPAGE TO CATEGORY
