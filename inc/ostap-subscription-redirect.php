<?php
// - OSTAP SUBSCRIPTION REDIRECT
function subscription_redirect_post() {
    global $wp_query;
    global $enhanced_category;

    if ( $wp_query->is_single() && isset($wp_query->query['post_type'] ) && 'ecp' ==  $wp_query->query['post_type'] ) {
        $category_link = get_category_link( $enhanced_category->get_by_post(current($wp_query->posts)->ID)->category_id );
        wp_redirect( $category_link, 301 );
    }
}
add_action('template_redirect', 'subscription_redirect_post');

// - ENDS OSTAP SUBSCRIPTION REDIRECT