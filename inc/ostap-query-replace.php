<?php
// - OSTAP QUERY REPLACE
function fix_query_replace( $replacements, $options ) {
    global $wp_query;
    $sidebars = CustomSidebarsReplacer::get_options( 'modifiable' );
    $custom_sidebars_explain = CustomSidebarsExplain::instance();
    $expl = $custom_sidebars_explain->do_explain();
    if ( ! is_tax() && ! is_404() && ! is_category() && ! is_singular() && isset($wp_query->query['post_type']) && $wp_query->query['post_type'] != 'post' ) {
        // 4 |== Post-Tpe Archive ----------------------------------------------
        // `get_post_type() != 'post'` .. post-archive = post-index (see 7)

        $post_type = $wp_query->query['post_type'];

        $post_type = apply_filters( 'cs_replace_post_type', $post_type, 'archive' );
        $expl && do_action( 'cs_explain', 'Type 4: ' . ucfirst( $post_type ) . ' Archive' );

        if ( ! CustomSidebarsReplacer::supported_post_type( $post_type ) ) {
            $expl && do_action( 'cs_explain', 'Invalid post type, use default sidebars.' );
            return $options;
        }

        foreach ( $sidebars as $sb_id ) {
            if (
                isset( $options['post_type_archive'][ $post_type ] )
                && ! empty( $options['post_type_archive'][ $post_type ][ $sb_id ] )
            ) {

                $replacements[ $sb_id ] = array(
                    $options['post_type_archive'][ $post_type ][ $sb_id ],
                    'post_type_archive',
                    $post_type,
                );
            }
        }
    }
    return $replacements;
}

add_filter( 'cs_replace_sidebars', 'fix_query_replace', 10, 2 );
// - ENDS OSTAP QUERY REPLACE