<?php
//-OSTAP PRODUCTS FILTER
// Register and load the widget
function wpb_load_widget() {
    register_widget( 'amazingribs_reworked' );
    register_widget( 'thermometers_filter' );
}
add_action( 'widgets_init', 'wpb_load_widget' );

// Creating the widget
class amazingribs_reworked extends WP_Widget {

    function __construct() {

        parent::__construct(
            'amazingribs_reworked',
            __('Products Filter', 'amazingribs_reworked'),
            array( 'description' => __( 'Product Filter widget', 'amazingribs_reworked' ), )
        );
    }

// Creating widget front-end

    public function widget( $args, $instance ) {

        $title = apply_filters( 'widget_title', $instance['title'] );

// before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if ( ! empty( $title ) )
            echo $args['before_title'] . $title . $args['after_title'];
        echo "<p>Click Search to see all our Reviews and Ratings. Use the filters below to refine your search.</p>";
        echo "<hr>";
        $manufacturers = get_posts(array('post_type' => 'manufacturer', 'posts_per_page' => -1));

        echo "<form action=\"/products/\">
             <h3>Manufacturer</h3>
             <select name='manufacturer_id' style='width:100%'>";
                echo "<option value=''>- Any -</option>";
                foreach ($manufacturers as $manufacturer) {
                    echo '<option value="' . $manufacturer->ID . '">' . $manufacturer->post_title . '</option>';
                }
        echo "</select>";
        echo "<hr>";
        echo "<h3>Primary Function</h3>";
        $functions = get_terms(array('taxonomy' => 'function'));
        foreach ($functions as $function) {
            $checked = "";
            if(isset($_GET['function'])) {
                foreach ($_GET['function'] as $get) {
                    if($get == $function->term_id) {
                        $checked = "checked";
                    }
                }
            }
            echo '<label><input type="checkbox" name="function[]" '.$checked.' value="' . $function->term_id . '"/>' . $function->name. "</label><br>";
        }
        echo "<hr>";
        echo "<h3>Fuel</h3>";
        $terms = get_terms(array('taxonomy' => 'fuel'));
        foreach ($terms as $term) {
            $checked = "";
            if(isset($_GET['fuel'])) {
                foreach ($_GET['fuel'] as $get) {
                    if($get == $term->term_id) {
                        $checked = "checked";
                    }
                }
            }
            echo '<label><input type="checkbox" name="fuel[]" '.$checked.' value="' . $term->term_id . '"/>' . $term->name. "</label><br>";
        }
        echo "<hr>";
        echo "<h3>Price</h3>";
        foreach ($this->getPrice() as $price) {
            if($price[0] == 0) {
                $label = "Under $".$price[1];
            }
            else if($price[1] == 99999) {
                $label = "Over $".$price[0];
            } else {
                $label ="$".$price[0]." to $".$price[1];
            }
            $checked = "";
            if(isset($_GET['price'])) {
                foreach ($_GET['price'] as $get) {
                    if($get == $price[0]."-".$price[1]) {
                        $checked = "checked";
                    }
                }
            }
            echo '<label><input type="checkbox" name="price[]" '.$checked.' value="' . $price[0] .'-'.$price[1]. '"/>' .$label. "</label><br>";

        }

        echo "<hr>";
        echo "<h3>Capacity</h3>";
        echo '<label><input type="checkbox" '. $this->checkCapacity("0-320") .' name="capacity[]" value="0-320"/>Small: Under 320 sq. inches (up to 16 burgers*)</label><br>';
        echo '<label><input type="checkbox" '. $this->checkCapacity("321-640") .' name="capacity[]" value="321-640"/>Mid-Size: 321-640 sq. inches (up to 32 burgers*)</label><br>';
        echo '<label><input type="checkbox" '. $this->checkCapacity("641-1500") .' name="capacity[]" value="641-1500"/>Large: 641-1,500 sq. inches (up to 75 burgers*)</label><br>';
        echo '<label><input type="checkbox" '. $this->checkCapacity("1501-99999") .' name="capacity[]" value="1501-99999"/>Extra Large and Commercial: 1,501 sq. inches or more</label><br>';



        echo "<hr>";
        echo "<h3>Awards</h3>";
        $terms = get_terms(array('taxonomy' => 'awards'));
        foreach ($terms as $term) {
            $checked = "";
            if(isset($_GET['awards'])) {
                foreach ($_GET['awards'] as $get) {
                    if($get == $term->term_id) {
                        $checked = "checked";
                    }
                }
            }
            echo '<label><input type="checkbox" '.$checked.' name="awards[]" value="' . $term->term_id . '"/>' . $term->name. "</label><br>";
        }
        echo "<hr>";
        echo "<button type=\"submit\">Search</button>";
        echo "<hr>";
        echo "</form> ";

        echo $args['after_widget'];
    }

    public function checkCapacity($capacity) {
        $checked = "";
        if(isset($_GET['capacity'])) {
            foreach ($_GET['capacity'] as $get) {
                if($get == $capacity) {
                    $checked = "checked";
                }
            }
        }
        return $checked;
    }
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'amazingribs_reworked' );
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }


    public function getPrice() {
        $prices = [
            ["0", "200"],
            ["200", "399"],
            ["400", "599"],
            ["600", "799"],
            ["800", "999"],
            ["1000", "1999"],
            ["2000", "3499"],
            ["3500", "6999"],
            ["7000", "99999"],

        ];
        return $prices;
    }
}



// Creating the widget
class thermometers_filter extends WP_Widget
{

    function __construct()
    {
        parent::__construct(
            'thermometers_filter',
            __('Thermometers Filter', 'amazingribs_reworked'),
            array('description' => __('Thermometers Filter widget', 'amazingribs_reworked'),)
        );
    }

// Creating widget front-end

    public function widget($args, $instance)
    {
        $title = apply_filters('widget_title', $instance['title']);

// before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];
        echo "<p>Click Search to see all our Reviews and Ratings. Use the filters below to refine your search.</p>";
        echo "<hr>";
        $manufacturers = get_posts(
                array(
                'post_type' => 'manufacturer',
                'posts_per_page' => -1,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'review_type',
                        'field' => 'slug',
                        'terms' => "thermometers" // Where term_id of Term 1 is "1".
                    )
                )
            )
        );


        echo "<form action=\"/thermometer_reviews/\">
             <h3>Manufacturer</h3>
             <select name='manufacturer_id' style='width:100%'>";
        echo "<option value=''>- Any -</option>";
        foreach ($manufacturers as $manufacturer) {
            echo '<option value="' . $manufacturer->ID . '">' . $manufacturer->post_title . '</option>';
        }
        echo "</select>";
        echo "<hr>";
        echo "<h3>Thermometer Function</h3>";
        $functions = get_terms(array('taxonomy' => 'function_thermometers'));
        foreach ($functions as $function) {
            $checked = "";
            if (isset($_GET['function'])) {
                foreach ($_GET['function'] as $get) {
                    if ($get == $function->term_id) {
                        $checked = "checked";
                    }
                }
            }
            echo '<label><input type="checkbox" name="function[]" ' . $checked . ' value="' . $function->term_id . '"/>' . $function->name . "</label><br>";
        }
        echo "<hr>";
        echo "<h3>Awards</h3>";
        $terms = get_terms(array('taxonomy' => 'awards'));
        foreach ($terms as $term) {
            $checked = "";
            if (isset($_GET['awards'])) {
                foreach ($_GET['awards'] as $get) {
                    if ($get == $term->term_id) {
                        $checked = "checked";
                    }
                }
            }
            echo '<label><input type="checkbox" ' . $checked . ' name="awards[]" value="' . $term->term_id . '"/>' . $term->name . "</label><br>";
        }
        echo "<hr>";
        echo "<h3>Thermometer Bundles</h3>";
        $terms = get_terms(array('taxonomy' => 'thermometer_bundles'));
        foreach ($terms as $term) {
            $checked = "";
            if (isset($_GET['bundles'])) {
                foreach ($_GET['bundles'] as $get) {
                    if ($get == $term->term_id) {
                        $checked = "checked";
                    }
                }
            }
            echo '<label><input type="checkbox" name="bundles[]" ' . $checked . ' value="' . $term->term_id . '"/>' . $term->name . "</label><br>";
        }
        echo "<hr>";
        echo "<button type=\"submit\">Search</button>";
        echo "<hr>";
        echo "</form> ";

        echo $args['after_widget'];
    }


    public function form($instance)
    {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('New title', 'amazingribs_reworked');
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>"/>
        </p>
        <?php
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}



function amazingribs_reworked( $query ) {
    if(!$query->is_admin && isset($query->query['post_type'] )) {
        if ($query->query['post_type'] == "products" && $query->is_archive && !$query->is_admin) {
            $taxquery = [];
            $metaQuery = [];
            if (isset($_GET['fuel'])) {
                $taxquery[] = array(
                    'taxonomy' => 'fuel',
                    'field' => 'id',
                    'terms' => $_GET['fuel'],
                    'operator' => 'IN'
                );
            }
            if (isset($_GET['awards'])) {
                $taxquery[] = array(
                    'taxonomy' => 'awards',
                    'field' => 'id',
                    'terms' => $_GET['awards'],
                    'operator' => 'IN'
                );
            }
            if (isset($_GET['function'])) {
                $taxquery[] = array(
                    'taxonomy' => 'function',
                    'field' => 'id',
                    'terms' => $_GET['function'],
                    'operator' => 'IN'
                );
            }

            if (isset($_GET['price'])) {
                $priceMeta = ['relation' => "OR"];
                foreach ($_GET['price'] as $price) {
                    $price = explode("-", $price);
                    $priceMeta[] = [
                        'relation' => 'AND',
                        array(
                            'key' => 'wpcf-price',
                            'value' => $price[0],
                            'compare' => '>=',
                            'type' => 'numeric'
                        ),
                        array(
                            'key' => 'wpcf-price',
                            'value' => $price[1],
                            'compare' => '<=',
                            'type' => 'numeric'
                        ),
                    ];
                }
                $metaQuery[] = $priceMeta;
            }

            if (isset($_GET['capacity'])) {
                $capacityMeta = ['relation' => "OR"];
                foreach ($_GET['capacity'] as $capacity) {
                    $capacity = explode("-", $capacity);
                    $capacityMeta[] = [
                        'relation' => 'AND',
                        array(
                            'key' => 'wpcf-primary_capacity',
                            'value' => $capacity[0],
                            'compare' => '>=',
                            'type' => 'numeric'
                        ),
                        array(
                            'key' => 'wpcf-primary_capacity',
                            'value' => $capacity[1],
                            'compare' => '<=',
                            'type' => 'numeric'
                        ),
                    ];
                }
                $metaQuery[] = $capacityMeta;
            }

            if (!empty($taxquery)) {
                $taxquery['relation'] = 'AND';
            }

            if (!empty($metaQuery)) {
                $metaQuery['relation'] = 'AND';
            }
            $query->set('tax_query', $taxquery);
            if (isset($_GET['manufacturer_id']) && !empty($_GET['manufacturer_id'])) {

                $connected_posts = toolset_get_related_posts(
                    [
                        'parent' => [$_GET['manufacturer_id']],
                    ],
                    'manufacturer-name-products-manufacturer'
                );
                $query->set('post__in', $connected_posts);
            }

            $query->set('meta_query', $metaQuery);

        }
        if ($query->query['post_type'] == "thermometer_reviews" && $query->is_archive && !$query->is_admin) {
            $taxquery = [];
            $metaQuery = [];
            if (isset($_GET['bundles'])) {
                $taxquery[] = array(
                    'taxonomy' => 'thermometer_bundles',
                    'field' => 'id',
                    'terms' => $_GET['bundles'],
                    'operator' => 'IN'
                );
            }
            if (isset($_GET['awards'])) {
                $taxquery[] = array(
                    'taxonomy' => 'awards',
                    'field' => 'id',
                    'terms' => $_GET['awards'],
                    'operator' => 'IN'
                );
            }
            if (isset($_GET['function'])) {
                $taxquery[] = array(
                    'taxonomy' => 'function_thermometers',
                    'field' => 'id',
                    'terms' => $_GET['function'],
                    'operator' => 'IN'
                );
            }


            if (!empty($taxquery)) {
                $taxquery['relation'] = 'AND';
            }

            if (!empty($metaQuery)) {
                $metaQuery['relation'] = 'AND';
            }
            $query->set('tax_query', $taxquery);
            if (isset($_GET['manufacturer_id']) && !empty($_GET['manufacturer_id'])) {

                $connected_posts = toolset_get_related_posts(
                    [
                        'parent' => [$_GET['manufacturer_id']],
                    ],
                    'manufacturer-thermo-thermometer_reviews-manufacturer'
                );
                $query->set('post__in', $connected_posts);
            }

            $query->set('meta_query', $metaQuery);

        }
    }
    return $query;
}


add_action( 'pre_get_posts', 'amazingribs_reworked' );
//-ENDS OSTAP PRODUCTS FILTER