<?php
//- OSTAP COLOR FILTER
add_filter('relevanssi_hits_filter', 'relevanssi_colour_filter');
function relevanssi_colour_filter($hits) {
    global $wp_query;
    if (isset($wp_query->query_vars['tag']) && !empty($wp_query->query_vars['tag'])) {
        $tagged = array();
        $tags = explode(",", $wp_query->query_vars['tag']);
        foreach ($hits[0] as $hit) {
            foreach ($tags as $tag) {
                if (has_term($tag, 'post_tag', $hit->ID)) {
                    $tagged[] = $hit;
                    break;
                }
            }
        }
        $hits[0] = $tagged;
    }
    return $hits;
}

//- ENDS OSTAP COLOR FILTER