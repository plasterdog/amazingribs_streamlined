<?php
//JMC widget styling

// JMC-Allows extra HTML items in to the_excerpt instead of stripping them like WordPress does
function theme_t_wp_improved_trim_excerpt($text) {
    global $post;
    if ( '' == $text ) {
        $text = get_the_content('');
        $text = apply_filters('the_content', $text);
        $text = str_replace(']]>', ']]&gt;', $text);
        $text = preg_replace('@<script[^>]*?>.*?</script>@si', '', $text);
        $text = strip_tags($text, '<p>,<ul>,<li>,<ol>');
        $excerpt_length = 55;
        $words = explode(' ', $text, $excerpt_length + 1);
        if (count($words)> $excerpt_length) {
            array_pop($words);
            array_push($words, '[...]');
            $text = implode(' ', $words);
        }
    }
    return $text;
}
 
remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'theme_t_wp_improved_trim_excerpt');

/* JMC- remove html filters from category descriptions*/
$filters = array( 'pre_term_description' );

foreach ( $filters as $filter ) {
    remove_filter( $filter, 'wp_filter_kses' );
}

foreach ( array( 'term_description' ) as $filter ) {
    remove_filter( $filter, 'wp_kses_data' );
}

// JMC - allows shortcodes in widgets
add_filter('widget_text', 'do_shortcode');



//JMC disable WordPress sanitization to allow more than just $allowedtags from /wp-includes/kses.php
//http://badlywired.com/2015/03/allow-html-in-user-description/     
remove_filter('pre_user_description', 'wp_filter_kses');
//add sanitization for WordPress posts
add_filter( 'pre_user_description', 'wp_filter_post_kses');
