<?php

//require_once( __DIR__ . '/inc/ostap-encoding-filter.php');
//require_once( __DIR__ . '/inc/ostap-not-certain.php');
//require_once( __DIR__ . '/inc/ostap-color-filter.php');
//require_once( __DIR__ . '/inc/ostap-products-filter.php');
//require_once( __DIR__ . '/inc/ostap-subscription-redirect.php');
//require_once( __DIR__ . '/inc/ostap-query-replace.php');
//require_once( __DIR__ . '/inc/ostap-homepage-to-category.php');
require_once( __DIR__ . '/inc/customizer.php');
require_once( __DIR__ . '/inc/widget-styling.php');
require_once( __DIR__ . '/inc/backend-experience.php');

/*--- PLASTERDOG INCLUSION --*/

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
    $content_width = 640; /* pixels */
}

if ( ! function_exists( 'amazingribs_reworked_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function amazingribs_reworked_setup() {

    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on amazingribs_reworked, use a find and replace
     * to change 'amazingribs_reworked' to the name of your theme in all the template files
     */
    load_theme_textdomain( 'amazingribs_reworked', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );
    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
     */
    //add_theme_support( 'post-thumbnails' );
    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
        'primary' => __( 'Primary Menu', 'amazingribs_reworked' ),

    ) );
    // Setup the WordPress core custom background feature.
    add_theme_support( 'custom-background', apply_filters( 'amazingribs_reworked_custom_background_args', array(
        'default-color' => 'ffffff',
        'default-image' => '',
    ) ) );


    // Enable support for HTML5 markup.
    add_theme_support( 'html5', array(
        'comment-list',
        'search-form',
        'comment-form',
        'gallery',
    ) );
}
endif; // amazingribs_reworked_setup
add_action( 'after_setup_theme', 'amazingribs_reworked_setup' );


//DETERMINES WHICH CUSTOMIZER PANELS ARE HIDDEN
//https://wordpress.stackexchange.com/questions/250349/how-to-remove-menus-section-from-wordpress-theme-customizer

function mytheme_customize_register( $wp_customize ) {
  //All our sections, settings, and controls will be added here

  //$wp_customize->remove_section( 'title_tagline');
  $wp_customize->remove_section( 'background_image');
  $wp_customize->remove_panel( 'nav_menus');
  $wp_customize->remove_panel( 'widgets');
  $wp_customize->remove_section( 'static_front_page');
}
add_action( 'customize_register', 'mytheme_customize_register',50 );

/*--- ENDS PLASTERDOG INCLUSION --*/



function style_scripts() {
    // Get the style.css timestamp & make it a named constant
    $css_timestamp = filemtime( get_stylesheet_directory().'/style.css' );
    define( 'THEME_VERSION', $css_timestamp );

// Use it in your enqueues
    wp_enqueue_style(
        'style_name',
        get_stylesheet_directory_uri() . '/style.css',
        array(),
        THEME_VERSION
    );
}

add_action( 'wp_enqueue_scripts', 'style_scripts' );
function filter_search($query) {
    if ($query->is_search && !$query->is_admin) {
        $query->set('post_type', array('post', 'grill_products_type', 'thermometer_reviews', 'tools', 'ecp'));
    };
    if ($query->is_category && !$query->is_admin) {
        $query->set('post_type', array('post', 'products', 'thermometer_reviews', 'tools', 'ecp'));
    };
    return $query;
};
add_filter('pre_get_posts', 'filter_search');

function amazingribs_reworked_scripts() {

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'amazingribs_reworked_scripts' );

//PLASTERDOG https://remicorson.com/include-all-your-wordpress-custom-post-types-in-search/
function rc_add_cpts_to_search($query) {

    // Check to verify it's search page
    if( is_search() ) {
        // Get post types
        $post_types = get_post_types(array('public' => true, 'exclude_from_search' => false), 'objects');
        $searchable_types = array();
        // Add available post types
        if( $post_types ) {
            foreach( $post_types as $type) {
                $searchable_types[] = $type->name;
            }
        }
        $query->set( 'post_type', $searchable_types );
    }
    return $query;
}
add_action( 'pre_get_posts', 'rc_add_cpts_to_search' );


//PLASTERDOG https://carriedils.com/add-editor-style/
add_action( 'init', 'cd_add_editor_styles' );
/**
 * Apply theme's stylesheet to the visual editor.
 *
 * @uses add_editor_style() Links a stylesheet to visual editor
 * @uses get_stylesheet_uri() Returns URI of theme stylesheet
 */
function cd_add_editor_styles() {
 add_editor_style( get_stylesheet_uri() );
}


// WIDGET REGIONS
function my_custom_sidebar() {

//THESE ITEMS SHOW ON THE LEFT SIDE    




    //LEGACY
    register_sidebar(
        array (
            'name' => __( 'Left Post Side Bar', 'amazingribs_reworked' ),
            'id' => 'left-side-bar',
            'description'   => 'For single posts and category archives',
            'before_widget' => '<div class="widget-left-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );

    //PDOG ADDITION
    register_sidebar(
        array (
            'name' => __( 'Left Page Sidebar', 'amazingribs_reworked' ),
            'id' => 'left-page',
            'description' => 'Left sidebar for page content',
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );

    //PDOG ADDITION
    register_sidebar(
        array (
            'name' => __( 'Left Manufacturer Sidebar', 'amazingribs_reworked' ),
            'id' => 'left-man',
            'description' => 'Left sidebar for manufacturer content',
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );

    //PDOG ADDITION
    register_sidebar(
        array (
            'name' => __( 'Left Product Sidebar', 'amazingribs_reworked' ),
            'id' => 'left-prod',
            'description' => 'Left sidebar for product content',
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );

    //PDOG ADDITION
    register_sidebar(
        array (
            'name' => __( 'Left Tool Sidebar', 'amazingribs_reworked' ),
            'id' => 'left-tool',
            'description' => 'Left sidebar for tool content',
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );

    //PDOG ADDITION
    register_sidebar(
        array (
            'name' => __( 'Left Review Sidebar', 'amazingribs_reworked' ),
            'id' => 'left-review',
            'description' => 'Left sidebar for review content',
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );

    //PDOG ADDITION
    register_sidebar(
        array (
            'name' => __( 'Left Author Sidebar', 'amazingribs_reworked' ),
            'id' => 'left-author',
            'description' => 'Left sidebar for author content',
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );

    //PDOG ADDITION
    register_sidebar(
        array (
            'name' => __( 'Header Call Out', 'amazingribs_reworked' ),
            'id' => 'header-callout',
            'description' => 'header call out note that images canno be over 185px',
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    //LEGACY
    register_sidebar(
        array (
            'name' => __( 'Lower Left Footer', 'amazingribs_reworked' ),
            'id' => 'footer1',
            'description' => 'Lower Left Footer',
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );

//THESE ITEMS SHOW ON THE RIGHT SIDE 


    //LEGACY
    register_sidebar(
        array (
            'name' => __( 'Right Post Side Bar', 'amazingribs_reworked' ),
            'id' => 'right-side-bar',
            'description'   => 'For single posts and category archives',
            'before_widget' => '<div class="widget-right-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    //PDOG ADDITION
    register_sidebar(
        array (
            'name' => __( 'Right Page Sidebar', 'amazingribs_reworked' ),
            'id' => 'right-page',
            'description' => 'right sidebar for page content',
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );


    //PDOG ADDITION
    register_sidebar(
        array (
            'name' => __( 'Right Manufacturer Sidebar', 'amazingribs_reworked' ),
            'id' => 'right-man',
            'description' => 'Right sidebar for manufacturer content',
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );

    //PDOG ADDITION
    register_sidebar(
        array (
            'name' => __( 'Right Product Sidebar', 'amazingribs_reworked' ),
            'id' => 'right-prod',
            'description' => 'Right sidebar for product content',
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );

    //PDOG ADDITION
    register_sidebar(
        array (
            'name' => __( 'Right Tool Sidebar', 'amazingribs_reworked' ),
            'id' => 'right-tool',
            'description' => 'Right sidebar for tool content',
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );

    //PDOG ADDITION
    register_sidebar(
        array (
            'name' => __( 'Right Review Sidebar', 'amazingribs_reworked' ),
            'id' => 'right-review',
            'description' => 'Right sidebar for review content',
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );

    //PDOG ADDITION
    register_sidebar(
        array (
            'name' => __( 'Right Author Sidebar', 'amazingribs_reworked' ),
            'id' => 'right-author',
            'description' => 'Right sidebar for author content',
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    

    //PDOG ADDITION
    register_sidebar(
        array (
            'name' => __( 'Upper Footer', 'amazingribs_reworked' ),
            'id' => 'upper-footer',
            'description' => 'Upper Footer',
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );

    //LEGACY
    register_sidebar(
        array (
            'name' => __( 'Lower Right Footer', 'amazingribs_reworked' ),
            'id' => 'footer2',
            'description' => 'Lower Right Footer',
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
}
// - ENDS WIDGET REGIONS


// - OSTAP FIRST PAGINATION SET
function pagination_bar() {
    global $wp_query;

    $total_pages = $wp_query->max_num_pages;

    if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));

        echo paginate_links(array(
            'base' => @add_query_arg( 'paged', '%#%' ),
            'format' => '',
            'current' => $current_page,
            'total' => $total_pages,
        ));
    }
}

add_action( 'widgets_init', 'my_custom_sidebar' );

function remove_page_from_query_string($query_string)
{
    if(!is_admin() && isset($query_string['name'])) {
        if ($query_string['name'] == 'page' && isset($query_string['page'])) {
            unset($query_string['name']);
            $query_string['paged'] = $query_string['page'];
        }
    }
    return $query_string;
}
add_filter('request', 'remove_page_from_query_string');

function wpse_modify_category_query( $query ) {
    if ( get_query_var('paged') ) {
        $paged = get_query_var('paged');
    } elseif ( get_query_var('page') ) { // 'page' is used instead of 'paged' on Static Front Page
        $paged = get_query_var('page');
    } else {
        $paged = 1;
    }
    if ( $query->is_category() ) {
        $query->set( 'paged', $paged );
    }
}

// - ENDS OSTAP FIRST PAGINATION SET

add_action( 'pre_get_posts', 'wpse_modify_category_query' );

add_theme_support( 'post-thumbnails',array('page', 'post', 'ecp', 'products', 'thermometer_reviews', 'tools', 'manufacturer'));
add_theme_support( 'post_tag',array('page', 'post', 'ecp', 'products', 'thermometer_reviews', 'tools', 'manufacturer'));



// - OSTAP SECOND PAGINATION SET
function custom_pagination() {

    $settings = array(
        'count' => 6,
        'prev_text' => "Previous |",
        'next_text' => "| Next"
    );

    global $wp_query;
    $current = max( 1, get_query_var( 'paged' ) );
    $currentbox = $current;
    $total = $wp_query->max_num_pages;
    $links = array();



    // Previous
    if( $current > 1 ) {
     //   $settings['count']--;
        $links[] = custom_pagination_link( 1, 'first', "First | " );
        $links[] = custom_pagination_link( $current - 1, 'prev', $settings['prev_text'] );
    }

    if($current < ($settings['count']/2)) {
        $currentbox = 1;
    } else if(($settings['count']/2) > ($total - $current)) {
        $currentbox = ($total - $settings['count'] + 1);
    } else {
        $currentbox = $currentbox - ($settings['count']/2);
    }


    // Next Pages
    for( $i = 0; $i < $settings['count']; $i++ ) {
        $page = $currentbox + $i;
        if( $page <= $total ) {
            if($page == $current) {
                $links[] = custom_pagination_link( $page,  'current' );
            } else {
                $links[] = custom_pagination_link($page);
            }
        }
    }

    // Next
    if( $current < $total ) {
        $links[] = custom_pagination_link( $current + 1, 'next', $settings['next_text'] );
        $links[] = custom_pagination_link( $total, 'last', " | Last" );

    }


    echo '<nav class="pagination">';
    echo join( ' ', $links );
    echo '</nav>';
}
add_action( 'custom_pagination', 'custom_pagination' );

function custom_pagination_link( $page = false, $class = '', $label = '' ) {

    if( ! $page )
        return;

    $classes = array( 'page-numbers' );
    if( !empty( $class ) )
        $classes[] = $class;
    $classes = array_map( 'sanitize_html_class', $classes );

    $label = $label ? $label : $page;
    $link = esc_url_raw( get_pagenum_link( $page ) );

    return '<a class="' . join ( ' ', $classes ) . '" href="' . $link . '">' . $label . '</a>';

}
// - ENDS SECOND PAGINATION SET


//ALLOWING AN OPTIONS PAGE | https://www.advancedcustomfields.com/resources/acf_add_options_page/

if( function_exists('acf_add_options_page') ) {
acf_add_options_page( ) ;
    acf_add_options_sub_page('404 message'); 
}

add_action( 'after_setup_theme', 'wpse_theme_setup' );
function wpse_theme_setup() {
    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );
}

// PLASTERDOG | PULLING IN THE SCRIPTS THAT WERE INCORRECTLY REFERENCED IN THE HEADER | https://scanwp.net/blog/learn-how-to-add-style-and-script-files-to-wordpress-correctly/

function custom_scripts() {
wp_enqueue_style( 'font-awesome-style' , 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' );
wp_enqueue_script( 'custom-jquery-script', 'https://code.jquery.com/jquery-3.4.1.min.js', array( 'jquery' ), false, true );

}
add_action( 'wp_enqueue_scripts', 'custom_scripts' );