<?php 
/**
 *Template Name: Tool Array
 * @package amazingribs_reworked
 */
get_header('nocrumb');?>

<?php
if ( have_posts() ) : while ( have_posts() ): the_post(); ?>

    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <h1><?php the_title(); ?></h1>
      <!-- there is no published date on this page type since it makes no sense for a dynamic page -->
        <div class="post-excerpt">
          <?php the_content(); ?>

              <?php
      wp_link_pages( array(
        'before' => '<div class="page-links">' . __( 'Pages:', 'amazingribs_reworked' ),
        'after'  => '</div>',
      ) );
    ?>
        </div><!-- ends post excerpt container div -->

   </div><!-- ends the post content container -->

<?php endwhile;
endif;
?>
    <nav class="pagination">
        <?php pagination_bar(); ?>
    </nav>
<!-- ALL OF THIS USED TO BE IN THE FOOTER -->
</div><!-- /.row -->


<section class="comments">
   <?php if ( is_active_sidebar( 'upper-footer' ) ) : ?>
          <?php dynamic_sidebar( 'upper-footer' ); ?> 
<?php endif; ?>
</section>

</div><!-- ends container -->


<!-- CONDITION FOR ALTERNATE SIDEBAR -->
<div class="widget-section widget-left" id="amazing-filter">
    
                  <?php if ( ! dynamic_sidebar( 'left-tool' ) ) : ?>
                  <?php endif; // end sidebar widget area ?>
        
</div>

<!-- ENDS CONDITION FOR ALTERNATE SIDEBAR -->
<!-- https://webprosmeetup.org/wp_is_mobile/ -->
<?php if ( wp_is_mobile() ) : ?>
<!-- nothing here we are dropping the right sidebar for mobile devices -->
<?php else : ?>
    <?php if ( is_active_sidebar( 'right-tool' ) ) : ?>
      <div class="widget-section widget-right">
        <?php dynamic_sidebar( 'right-tool' ); ?>
      </div>
    <?php endif; ?>
<?php endif; ?>

</div><!-- row? -->
</div><!-- container? -->
<!-- NOW GET THE MODIFIED FOOTER -->
<?php get_footer(); ?>