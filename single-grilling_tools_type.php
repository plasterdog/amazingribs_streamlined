<?php 
/**
 *
 * @package amazingribs_ribs
 */
get_header('nocrumb');?>

<?php
if ( have_posts() ) : while ( have_posts() ): the_post(); ?>

    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php if ( get_field( 'alternate_title_over_ride' ) ): ?>
      <h1 class="single-item"><?php the_field('alternate_title_over_ride'); ?></h1>
    <?php else: // field_name returned false ?>
      <h1 class="single-item"> <?php the_title(); ?></h1>
<?php endif; // end of if field_name logic ?>

<!-- RELATED AUTHOR -->
<?php if ( get_field( 'related_author' ) ): ?>
  <p class="author-attribution intro">By:</p>
  <?php
  $related_author = get_field('related_author');
  if( $related_author ): ?>
  <?php foreach( $related_author as $post ): 
  // Setup this post for WP functions (variable must be named $post).
  setup_postdata($post); ?>
    <p class="author-attribution"><a href="<?php the_permalink(); ?>" rel="bookmark"> <?php the_title(); ?></a></p>
  <?php endforeach; ?>
  <?php 
  // Reset the global post object so that the rest of the page works correctly.
  wp_reset_postdata(); ?>
<?php endif; ?>
<?php else: // field_name returned false ?>
<!-- nothing right now-->

<?php endif; // end of if field_name logic ?>
 <div class="clear single-hero-image"><?php the_post_thumbnail( 'large' ); ?></div>

<!-- THE PRODUCT STATISTICS -->
            <ul>
            <?php if( get_field('item_has_been_discontinued') == 'yes' ) { ?>
            <li>This item has been discontinued</li>
            <?php } ?>
              
            <?php if( get_field('item_price') ): ?>
            <li>Price: <?php the_field('item_price'); ?></li>
            <?php endif; ?>


            <?php if( get_field('item_made_in_america') == 'yes' ) { ?>
            <li>Made in USA</li>
            <?php } ?>
            </ul>
<!-- ENDS THE PRODUCT STATISTICS -->

          <?php the_content(); ?>

<div class="clear"><!-- the container for the quote and award pair -->
<!--- THE QUOTATION SECTION -->  
  <?php if ( get_field( 'amazing_ribs_awards' ) ): ?><!-- conditionally making the quotation section narrower -->
  <div class="two-thirds-left">
        <?php if ( get_field( 'relevant_quote' ) ): ?>
          <div class="amazing-quote"><aside><?php the_field('relevant_quote'); ?></aside></div><!-- ends amazing quote -->
        <?php else: // field_name returned false ?>
          <!-- reserved for a default quote or statement -->
        <?php endif; // end of if field_name logic ?>  
  </div><!-- ends two-thirds-left -->
  <?php else: // field_name returned false ?>

        <?php if ( get_field( 'relevant_quote' ) ): ?>
          <div class="amazing-quote"><aside><?php the_field('relevant_quote'); ?></aside></div><!-- ends amazing quote -->
        <?php else: // field_name returned false ?>
          <!-- reserved for a default quote or statement -->
        <?php endif; // end of if field_name logic ?>  

  <?php endif; // end of if field_name logic ?>
<!-- THE AWARD SECTION -->
        <?php if( get_field('amazing_ribs_awards') == 'queue' ) { ?></div>
            <div class="one-third-right"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/question-award.png" alt="not yet reviewed" /></div>
        <?php } ?>
        <?php if( get_field('amazing_ribs_awards') == 'no' ) { ?>
            <div class="one-third-right"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/bad-award.png" alt="not recommended" /></div>
        <?php } ?>
        <?php if( get_field('amazing_ribs_awards') == 'bronze' ) { ?>
            <div class="one-third-right"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/bronze-award.png" alt="Bronze Award" /></div>
        <?php } ?>
        <?php if( get_field('amazing_ribs_awards') == 'silver' ) { ?>
            <div class="one-third-right"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/silver-award.png" alt="Silver Award" /></div>
        <?php } ?>
        <?php if( get_field('amazing_ribs_awards') == 'gold' ) { ?>
            <div class="one-third-right"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/gold-award.png" alt="Gold Award" /></div>
        <?php } ?>
        <?php if( get_field('amazing_ribs_awards') == 'platinum' ) { ?>
            <div class="one-third-right"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/platinum-award.png" alt="Platinum Award" /></div>
        <?php } ?>
</div><!-- ends the quote and award pair -->

<div class="clear referring-link"><!--EXTERNAL LINK REPEATER SECTION -->
        <?php
        // check if the repeater field has rows of data
        if( have_rows('external_link') ): ?>
        <ul>
        <?php   // loop through the rows of data
            while ( have_rows('external_link') ) : the_row(); ?>
          <li>
          <?php the_sub_field('external_link_intro');?>: 
          <a href="<?php the_sub_field('external_link_url');?>" target="_blank">
          <?php the_sub_field('external_link_label');?></a>
          </li>
              <?php    endwhile; ?>
        </ul>
        <?php else :
            // no rows found
        endif; ?>
</div><!-- ends referring link -->
<!-- PUBLISHED INFO -->   
<p class="last-modified"><strong>Published On:</strong> <?php the_time('n/j/Y'); ?>   
<!--https://www.wpbeginner.com/wp-tutorials/display-the-last-updated-date-of-your-posts-in-wordpress/-->
<?php $u_time = get_the_time('U'); 
$u_modified_time = get_the_modified_time('U'); 
if ($u_modified_time >= $u_time + 86400) { 
echo "<strong>Last Modified:</strong> "; 
the_modified_time('n/j/Y'); 
 }  ?></p>
<!-- ENDS PUBLISHED INFO -->

<!-- related author link https://www.advancedcustomfields.com/resources/post-object/ -->
<?php
$related_author = get_field('related_author');
if( $related_author ): ?>

    <ul class="author-group">
    <?php foreach( $related_author as $post ): 

        // Setup this post for WP functions (variable must be named $post).
        setup_postdata($post); ?>
        <li>
    <div class="clear">

       <?php the_title(); ?> - <?php echo get_the_excerpt(); ?>
     
      </div><!-- ends clear-->
        </li>
       <hr class="author-seperator">
    <?php endforeach; ?>
    </ul>
    <?php 
    // Reset the global post object so that the rest of the page works correctly.
    wp_reset_postdata(); ?>
<?php endif; ?>
<!-- ends related author link -->

<!-- related manufacturer https://www.advancedcustomfields.com/resources/post-object/ -->
<?php
$related_manufacturer = get_field('related_manufacturer');
if( $related_manufacturer ): ?>
 <ul class="related-manufacturer"> <li>Manufacturer:</li>
    <?php foreach( $related_manufacturer as $post ): 
        // Setup this post for WP functions (variable must be named $post).
        setup_postdata($post); ?>     
        <li><?php the_title(); ?></li>
    <?php endforeach; ?>
    </ul>
    <?php 
    // Reset the global post object so that the rest of the page works correctly.
    wp_reset_postdata(); ?>
<?php endif; ?>
<!-- ends related manufacturer link -->



        <?php the_tags( __( 'Tags: ', 'amazingribs_reworked' ), ' ', '' ); ?>
      <?php edit_post_link( __( 'Edit', 'amazingribs_reworked' ), '<span class="edit-link">', '</span>' ); ?>  

                      <?php
                  // If comments are open or we have at least one comment, load up the comment template
                  if ( comments_open() || '0' != get_comments_number() ) :
                    comments_template();
                  endif;
                ?>
      
      

    </div><!-- ends the post content container -->

<?php endwhile;
endif;
?>
    <nav class="pagination">
        <?php pagination_bar(); ?>
    </nav>
<!-- ALL OF THIS USED TO BE IN THE FOOTER -->
</div><!-- /.row -->


<section class="comments">
   <?php if ( is_active_sidebar( 'upper-footer' ) ) : ?>
          <?php dynamic_sidebar( 'upper-footer' ); ?> 
<?php endif; ?>
</section>

</div><!-- ends container -->


<!-- CONDITION FOR ALTERNATE SIDEBAR -->
<div class="widget-section widget-left" id="amazing-filter">
          <?php if ( get_field( 'alternate_sidebar_over_ride' ) ): ?>
                     <?php the_field('alternate_sidebar_over_ride'); ?>
          <?php else: // field_name returned false ?>
                  <?php if ( ! dynamic_sidebar( 'left-tool' ) ) : ?>
                  <?php endif; // end sidebar widget area ?>
          <?php endif; // end of if field_name logic ?>
</div>

<!-- ENDS CONDITION FOR ALTERNATE SIDEBAR -->

<!-- https://webprosmeetup.org/wp_is_mobile/ -->
<?php if ( wp_is_mobile() ) : ?>
<!-- nothing here we are dropping the right sidebar for mobile devices -->
<?php else : ?>
    <?php if ( is_active_sidebar( 'right-tool' ) ) : ?>
      <div class="widget-section widget-right">
        <?php dynamic_sidebar( 'right-tool' ); ?>
      </div>
    <?php endif; ?>
<?php endif; ?>

</div><!-- row? -->
</div><!-- container? -->
<!-- NOW GET THE MODIFIED FOOTER -->
<?php get_footer(); ?>