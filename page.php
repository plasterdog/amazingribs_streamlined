<?php 
/**
 *
 * @package amazingribs_reworked
 */
get_header();?>

<?php
if ( have_posts() ) : while ( have_posts() ): the_post(); ?>

    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

      <h1><?php the_title(); ?></h1>

<!-- PUBLISHED INFO -->   
<p class="last-modified">published on: <?php the_time('l, F j, Y'); ?>   </p>
<!-- https://pagely.com/blog/display-post-last-updated-wordpress/ -->
<?php
function show_last_modified_date( $content ) {
$original_time = get_the_time('U');
$modified_time = get_the_modified_time('U');
if ($modified_time >= $original_time + 86400) {
$updated_time = get_the_modified_time('h:i a');
$updated_day = get_the_modified_time('l, F j, Y');
$modified_content .= '<p class="last-modified">updated on '. $updated_day . '</p>';
}
$modified_content .= $content;
return $modified_content;
}
add_filter( 'the_content', 'show_last_modified_date' );
?>
<!-- ENDS PUBLISHED INFO -->

        <div class="post-excerpt">
          <?php the_content(); ?>

              <?php
      wp_link_pages( array(
        'before' => '<div class="page-links">' . __( 'Pages:', 'amazingribs_reworked' ),
        'after'  => '</div>',
      ) );
    ?>

        </div><!-- ends post excerpt container div -->

    </div><!-- ends the post content container -->

<?php endwhile;
endif;
?>
    <nav class="pagination">
        <?php pagination_bar(); ?>
    </nav>
<!-- ALL OF THIS USED TO BE IN THE FOOTER -->
</div><!-- /.row -->


<section class="comments">
   <?php if ( is_active_sidebar( 'upper-footer' ) ) : ?>
          <?php dynamic_sidebar( 'upper-footer' ); ?> 
<?php endif; ?>
</section>

</div><!-- ends container -->


<!-- CONDITION FOR ALTERNATE SIDEBAR -->
<div class="widget-section widget-left">
    
                  <?php if ( ! dynamic_sidebar( 'left-page' ) ) : ?>
                  <?php endif; // end sidebar widget area ?>
        
</div>

<!-- ENDS CONDITION FOR ALTERNATE SIDEBAR -->


    <?php if ( is_active_sidebar( 'right-page' ) ) : ?>
      <div class="widget-section widget-right">
        <?php dynamic_sidebar( 'right-page' ); ?>
      </div>
    <?php endif; ?>


</div><!-- row? -->
</div><!-- container? -->
<!-- NOW GET THE MODIFIED FOOTER -->
<?php get_footer(); ?>