<?php
/**
 * The header for our theme.
 *
 * @package amazingribs_reworked
 *
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <?php wp_head(); ?>
    <?php if (function_exists('the_ad_placement')) {
        the_ad_placement('head');
    } ?>
    <script async src="//www.googletagservices.com/tag/js/gpt.js"></script>
    <script async src="//acdn.adnxs.com/prebid/not-for-prod/1/prebid.js"></script>
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WGBS7ZG"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

</head>

<body <?php body_class(); ?>>
        <?php
        wp_body_open();
        ?>
   <div class="wrapper">
        <div id="main-body-wedge" class="no-ads"></div>
        <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
            <div class="container-fluid wrapper">

                <div class="navbar-header page-scroll">
                    <!-- change the action to /ar2/ on dev site -->
                    <a href="<?php echo get_home_url(); ?>" rel="home" class="navbar-brand">
                        <img alt="AmazingRibs.com Logo" id="logo" src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo.png" title="AmazingRibs logo"/>
                    </a>
                    <p class="tagline"><?php echo get_bloginfo( 'description' ); ?> </p>
                </div>
                <div class="mobileMenu">
                    <input type="checkbox" class="menuBurger"/>

                    <!--
                    Some spans to act as a hamburger.

                    They are acting like a real hamburger,
                    not that McDonalds stuff.
                    -->
                    <span class="bar"></span>
                    <span class="bar"></span>
                    <span class="bar"></span>
                    <div class="collapse navbar-collapse" id="menuToggle">
                        <ul class="navbar-nav ml-auto" id="menu">
         <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
                        </ul>
                        
                        <div class="search">
                            <i class="fa fa-search"></i>
                            <div class="search-box">

                                <?php echo do_shortcode('[searchandfilter fields="search,post_types" types=",checkbox,checkbox" post_types="post,grill_products_type,grilling_tools_type" headings=",Look in one or more of these topics"]'); ?>

                            </div>
                        </div>
                    </div>
                </div>

                <!-- THIS IS WHERE THE ATTACHED HEADER AD IS SHOWING -->
               
           <?php if ( is_active_sidebar( 'header-callout' ) ) : ?>
          <div class="header-ads" ><?php dynamic_sidebar( 'header-callout' ); ?>  </div><!-- ends header ads -->
        <?php endif; ?>
               
            </div><!-- ends container fluid wrapper -->
        </nav>
        <div class="content-wrapper">
            <div id="center-body-wrapper" class="center-body">
                <div class="hero-image">
                    <a href="<?php echo get_home_url(); ?>"><img alt="AmazingRibs.com Logo and Masthead" data-noautostyle="1" data-nolazy="1" data-pin-nopin="true" id="masthead" src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/masthead018B.gif" title="Logo"></a>
                </div><!-- ends hero image -->
                <?php
                if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                }
                ?>
                <div class="container">
                    <div class="row">