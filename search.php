<?php 
/**
 *
 * @package amazingribs_reworked
 */
get_header(); ?>

<?php
if ( have_posts() ) : while ( have_posts() ): the_post(); ?>

    <div id="post-<?php the_ID(); ?>" class="post-item">


      <?php if ( get_field( 'alternate_featured_image_over_ride' ) ): ?>
            <a href="<?php the_permalink(); ?>" rel="bookmark">
        <img style="width:150px;" src="<?php the_field('alternate_featured_image_over_ride'); ?>"/></a>
      <?php else: // field_name returned false ?>
      <?php if ( has_post_thumbnail() ) { ?>
        <a href="<?php the_permalink(); ?>" rel="bookmark">
        <?php the_post_thumbnail( 'thumbnail' ); ?></a>
      <?php } else { ?>
        <a href="<?php the_permalink(); ?>" rel="bookmark">
        <img style="width:150px;" src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/default-thumb.jpg" alt="<?php the_title(); ?>" /></a>
        <?php } ?>  
      <?php endif; // end of if field_name logic ?>


        <div class="content">
            <?php if ( get_field( 'alternate_title_over_ride' ) ): ?>
            <h2 class="archive-array"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_field('alternate_title_over_ride'); ?></a></h2>
            <!-- AUTHOR ATTRIBUTION -->
  <?php if ( get_field( 'related_author' ) ): ?>
  <p class="author-attribution intro">Author:</p>
  <?php
  $related_author = get_field('related_author');
  if( $related_author ): ?>
  <?php foreach( $related_author as $post ): 
  // Setup this post for WP functions (variable must be named $post).
  setup_postdata($post); ?>
    <p class="author-attribution"> <?php the_title(); ?></p>
  <?php endforeach; ?>
  <?php 
  // Reset the global post object so that the rest of the page works correctly.
  wp_reset_postdata(); ?>
<?php endif; ?>
<?php else: // field_name returned false ?>
 <p class="author-attribution.intro">Author:<?php echo get_the_author_meta('display_name', $author_id); ?></p>

<?php endif; // end of if field_name logic ?>
<!-- ENDS AUTHOR ATTRIBUTION -->

        <?php else: // field_name returned false ?>
            <h2 class="archive-array"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
  
  <!-- AUTHOR ATTRIBUTION -->
  <?php if ( get_field( 'related_author' ) ): ?>
  <p class="author-attribution intro">Author:</p>
  <?php
  $related_author = get_field('related_author');
  if( $related_author ): ?>
  <?php foreach( $related_author as $post ): 
  // Setup this post for WP functions (variable must be named $post).
  setup_postdata($post); ?>
    <p class="author-attribution"> <?php the_title(); ?></p>
  <?php endforeach; ?>
  <?php 
  // Reset the global post object so that the rest of the page works correctly.
  wp_reset_postdata(); ?>
<?php endif; ?>
<?php else: // field_name returned false ?>
 <p class="author-attribution.intro">Author:<?php echo get_the_author_meta('display_name', $author_id); ?></p>

<?php endif; // end of if field_name logic ?>
<!-- ENDS AUTHOR ATTRIBUTION -->
          
        <?php endif; // end of if field_name logic ?>

            <?php the_excerpt(); ?>
            <p style="text-align:right;"><a href="<?php the_permalink(); ?>" rel="bookmark">... read more</a></p>

        </div>
    </div>

<?php endwhile;?>

<?php else : ?>
  <!-- THE SAME OPTIONS AS FOUND ON THE 404 PAGE -->
          <h1 class="page-title"><?php the_field('ar_404_title', 'option'); ?></h1>
        
        </header><!-- .page-header -->

        <?php the_field('ar_404_message_body', 'option'); ?>

          

<div class="clear" style="padding:2em 0; text-align:center;">
  <?php get_search_form(); ?>
</div>

<?php endif; ?>

    <nav class="pagination">
        <?php pagination_bar(); ?>
    </nav>
<!-- ALL OF THIS USED TO BE IN THE FOOTER -->
</div>
<!-- /.row -->
<section class="comments">
<?php if ( is_active_sidebar( 'upper-footer' ) ) : ?>
          <?php dynamic_sidebar( 'upper-footer' ); ?> 
<?php endif; ?>
</section>
</div>
<?php if ( is_active_sidebar( 'left-side-bar' ) ) : ?>
    <div class="widget-section widget-left">
       <?php dynamic_sidebar( 'left-side-bar' ); ?>
    </div>
<?php endif; ?>
<!-- /.container -->
</div>
<div class="widget-section widget-right">
    <?php if ( is_active_sidebar( 'right-side-bar' ) ) : ?>
      <?php dynamic_sidebar( 'right-side-bar' ); ?>
    <?php endif; ?>
</div>
</div>
<!-- NOW GET THE MODIFIED FOOTER -->
<?php get_footer(); ?>