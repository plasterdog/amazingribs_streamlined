<?php 
/**
 *
 * @package amazingribs_reworked
 */
get_header('nocrumb');?>

<?php
if ( have_posts() ) : while ( have_posts() ): the_post(); ?>

    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<!-- CONDITIONAL TITLE -->
    <?php if ( get_field( 'alternate_title_over_ride' ) ): ?>
      <h1 class="single-item"><?php the_field('alternate_title_over_ride'); ?></h1>
    <?php else: // field_name returned false ?>
      <h1 class="single-item"> <?php the_title(); ?></h1>
    <?php endif; // end of if field_name logic ?>
    
<!-- RELATED AUTHOR -->
<?php if ( get_field( 'related_author' ) ): ?>
  <p class="author-attribution intro">Author:</p>
  <?php
  $related_author = get_field('related_author');
  if( $related_author ): ?>
  <?php foreach( $related_author as $post ): 
  // Setup this post for WP functions (variable must be named $post).
  setup_postdata($post); ?>
    <p class="author-attribution"><a href="<?php the_permalink(); ?>" rel="bookmark"> <?php the_title(); ?></a></p>
  <?php endforeach; ?>
  <?php 
  // Reset the global post object so that the rest of the page works correctly.
  wp_reset_postdata(); ?>
<?php endif; ?>
<?php else: // field_name returned false ?>
 <!-- nothing right now-->

<?php endif; // end of if field_name logic ?>

  <div class="clear single-hero-image"><?php the_post_thumbnail( 'large' ); ?></div>

      <?php the_content(); ?>

<div class="clear"><!-- the container for the quote and award pair -->
<!--- THE QUOTATION SECTION -->  
  <?php if ( get_field( 'amazing_ribs_awards' ) ): ?><!-- conditionally making the quotation section narrower -->
  <div class="two-thirds-left">
        <?php if ( get_field( 'relevant_quote' ) ): ?>
          <div class="amazing-quote"><aside><?php the_field('relevant_quote'); ?></aside></div><!-- ends amazing quote -->
        <?php else: // field_name returned false ?>
          <!-- reserved for a default quote or statement -->
        <?php endif; // end of if field_name logic ?>  
  </div><!-- ends two-thirds-left -->
  <?php else: // field_name returned false ?>

        <?php if ( get_field( 'relevant_quote' ) ): ?>
          <div class="amazing-quote"><aside><?php the_field('relevant_quote'); ?></aside></div><!-- ends amazing quote -->
        <?php else: // field_name returned false ?>
          <!-- reserved for a default quote or statement -->
        <?php endif; // end of if field_name logic ?>  

  <?php endif; // end of if field_name logic ?>
<!-- THE AWARD SECTION -->
        <?php if( get_field('amazing_ribs_awards') == 'queue' ) { ?></div>
            <div class="one-third-right"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/question-award.png" alt="not yet reviewed" /></div>
        <?php } ?>
        <?php if( get_field('amazing_ribs_awards') == 'no' ) { ?>
            <div class="one-third-right"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/bad-award.png" alt="not recommended" /></div>
        <?php } ?>
        <?php if( get_field('amazing_ribs_awards') == 'bronze' ) { ?>
            <div class="one-third-right"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/bronze-award.png" alt="Bronze Award" /></div>
        <?php } ?>
        <?php if( get_field('amazing_ribs_awards') == 'silver' ) { ?>
            <div class="one-third-right"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/silver-award.png" alt="Silver Award" /></div>
        <?php } ?>
        <?php if( get_field('amazing_ribs_awards') == 'gold' ) { ?>
            <div class="one-third-right"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/gold-award.png" alt="Gold Award" /></div>
        <?php } ?>
        <?php if( get_field('amazing_ribs_awards') == 'platinum' ) { ?>
            <div class="one-third-right"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/platinum-award.png" alt="Platinum Award" /></div>
        <?php } ?>
</div><!-- ends the quote and award pair -->

<div class="clear"><!-- the statistics container -->
<div class="left-half">
 <!-- THE LEFT SIDE -->
            <ul>
            <?php if( get_field('item_has_been_discontinued') == 'yes' ) { ?>
            <li>This item has been discontinued</li>
            <?php } ?>
              
            <?php if( get_field('item_price') ): ?>
            <li><strong>Price:</strong> <?php the_field('item_price'); ?></li>
            <?php endif; ?>

            <?php if( get_field('item_made_in_america') == 'yes' ) { ?>
            <li>Made in USA</li>
            <?php } ?>

            <?php if( get_field('thermometer_app') ): ?>
            <li><strong>App:</strong> <?php the_field('thermometer_app'); ?></li>
            <?php endif; ?>

            <?php if( get_field('thermometer_probe') ): ?>
            <li><strong>Probe:</strong> <?php the_field('thermometer_probe'); ?></li>
            <?php endif; ?>

            <?php if( get_field('thermometer_cf_switch') ): ?>
            <li><strong>CF Switch:</strong> <?php the_field('thermometer_cf_switch'); ?></li>
            <?php endif; ?>

            <?php if( get_field('thermometer_backlight') ): ?>
            <li><strong>Backlight:</strong> <?php the_field('thermometer_backlight'); ?></li>
            <?php endif; ?>

            <?php if( get_field('thermometer_weight') ): ?>
            <li><strong>Weight:</strong> <?php the_field('thermometer_weight'); ?></li>
            <?php endif; ?>

            <?php if( get_field('thermometer_drop_test') ): ?>
            <li><strong>Drop Test:</strong> <?php the_field('thermometer_drop_test'); ?></li>
            <?php endif; ?>

            <?php if( get_field('thermometer_logging') ): ?>
            <li><strong>Logging:</strong> <?php the_field('thermometer_logging'); ?></li>
            <?php endif; ?>

            <?php if( get_field('additional_thermometer_features') ): ?>
            <li><strong>Additional Features:</strong> <?php the_field('additional_thermometer_features'); ?></li>
            <?php endif; ?>

            <?php if( get_field('thermometer_adjustable_options') ): ?>
            <li><strong>Adjustable Options:</strong> <?php the_field('thermometer_adjustable_options'); ?></li>
            <?php endif; ?>

            <?php if( get_field('included_with_thermometer') ): ?>
            <li><strong>Included:</strong> <?php the_field('included_with_thermometer'); ?></li>
            <?php endif; ?>

            <?php if( get_field('thermometer_available_accessories') ): ?>
            <li><strong>Available Accessories:</strong> <?php the_field('thermometer_available_accessories'); ?></li>
            <?php endif; ?>

            <?php if( get_field('thermometer_color_options') ): ?>
            <li><strong>Color Options:</strong> <?php the_field('thermometer_color_options'); ?></li>
            <?php endif; ?>
            </ul> 

</div><!-- ends left half -->
<div class="right-half">
<!-- THE RIGHT SIDE -->
<ul>
            <?php if( get_field('thermometer_display_precision') ): ?>
            <li><strong>Display Precision:</strong> <?php the_field('thermometer_display_precision'); ?></li>
            <?php endif; ?>

            <?php if( get_field('thermometer_size_numbers_display') ): ?>
            <li><strong>Numbers Display Size:</strong> <?php the_field('thermometer_size_numbers_display'); ?></li>
            <?php endif; ?>

            <?php if( get_field('thermometer_safe_operating_range') ): ?>
            <li><strong>Safe Operating Range:</strong> <?php the_field('thermometer_safe_operating_range'); ?></li>
            <?php endif; ?>

            <?php if( get_field('min_and_max_temp') ): ?>
            <li><strong>Min & Max Temp:</strong> <?php the_field('min_and_max_temp'); ?></li>
            <?php endif; ?>

            <?php if( get_field('32_actual_temp') ): ?>
            <li><strong>Actual Temp at 32 Degrees:</strong> <?php the_field('32_actual_temp'); ?></li>
            <?php endif; ?>

            <?php if( get_field('130_actual_temp') ): ?>
            <li><strong>Actual Temp at 130 Degrees:</strong> <?php the_field('130_actual_temp'); ?></li>
            <?php endif; ?>

            <?php if( get_field('225_actual_temp') ): ?>
            <li><strong>Actual Temp at 225 Degrees:</strong> <?php the_field('225_actual_temp'); ?></li>
            <?php endif; ?>

            <?php if( get_field('325_actual_temp') ): ?>
            <li><strong>Actual Temp at 325 Degrees:</strong> <?php the_field('325_actual_temp'); ?></li>
            <?php endif; ?>

            <?php if( get_field('max_actual_temp') ): ?>
            <li><strong>Actual Max Temp:</strong> <?php the_field('max_actual_temp'); ?></li>
            <?php endif; ?>

            <?php if( get_field('speed_32_to_212') ): ?>
            <li><strong>Speed from 32 to 212 Degrees:</strong> <?php the_field('speed_32_to_212'); ?></li>
            <?php endif; ?>

            <?php if( get_field('speed_212_to_325')): ?>
            <li><strong>Speed from 32 to 212 Degrees:</strong> <?php the_field('speed_212_to_325'); ?></li>
            <?php endif; ?>

            <?php if( get_field('thermometer_auto_shutoff')): ?>
            <li><strong>Auto Shutoff:</strong> <?php the_field('thermometer_auto_shutoff'); ?></li>
            <?php endif; ?>

            <?php if( get_field('thermometer_battery_type')): ?>
            <li><strong>Battery Type:</strong> <?php the_field('thermometer_battery_type'); ?></li>
            <?php endif; ?>

            <?php if( get_field('thermometer_battery_life')): ?>
            <li><strong>Battery Life:</strong> <?php the_field('thermometer_battery_life'); ?></li>
            <?php endif; ?>

            <?php if( get_field('water_resisitance_rating')): ?>
            <li><strong>Water Resistance Rating:</strong> <?php the_field('water_resisitance_rating'); ?></li>
            <?php endif; ?>

            <?php if( get_field('thermometer_alarms')): ?>
            <li><strong>Alarms:</strong> <?php the_field('thermometer_alarms'); ?></li>
            <?php endif; ?>
</ul>
</div><!-- ends right half -->
</div><!-- ends the statistics container -->

<div class="clear referring-link"><!--EXTERNAL LINK REPEATER SECTION -->
        <?php
        // check if the repeater field has rows of data
        if( have_rows('external_link') ): ?>
        <ul>
        <?php   // loop through the rows of data
            while ( have_rows('external_link') ) : the_row(); ?>
          <li>
          <?php the_sub_field('external_link_intro');?>: 
          <a href="<?php the_sub_field('external_link_url');?>" target="_blank">
          <?php the_sub_field('external_link_label');?></a>
          </li>
              <?php    endwhile; ?>
        </ul>
        <?php else :
            // no rows found
        endif; ?>
</div><!-- ends referring link -->
<!-- PUBLISHED INFO -->   
<p class="last-modified"><strong>Published On:</strong> <?php the_time('n/j/Y'); ?>   
<!--https://www.wpbeginner.com/wp-tutorials/display-the-last-updated-date-of-your-posts-in-wordpress/-->
<?php $u_time = get_the_time('U'); 
$u_modified_time = get_the_modified_time('U'); 
if ($u_modified_time >= $u_time + 86400) { 
echo "<strong>Last Modified:</strong> "; 
the_modified_time('n/j/Y'); 
 }  ?></p>
<!-- ENDS PUBLISHED INFO -->
<!-- related author link https://www.advancedcustomfields.com/resources/post-object/ -->
<?php
$related_author = get_field('related_author');
if( $related_author ): ?>

    <ul class="author-group">
    <?php foreach( $related_author as $post ): 

        // Setup this post for WP functions (variable must be named $post).
        setup_postdata($post); ?>
        <li>
    <div class="clear">

       <?php the_title(); ?> - <?php echo get_the_excerpt(); ?>
     
      </div><!-- ends clear-->
        </li>
       <hr class="author-seperator">
    <?php endforeach; ?>
    </ul>
    <?php 
    // Reset the global post object so that the rest of the page works correctly.
    wp_reset_postdata(); ?>
<?php endif; ?>
<!-- ends related author link -->

<!-- related manufacturer https://www.advancedcustomfields.com/resources/post-object/ -->
<?php
$related_manufacturer = get_field('related_manufacturer');
if( $related_manufacturer ): ?>
 <ul class="related-manufacturer"> <li>Manufacturer:</li>
    <?php foreach( $related_manufacturer as $post ): 
        // Setup this post for WP functions (variable must be named $post).
        setup_postdata($post); ?>     
        <li><?php the_title(); ?></li>
    <?php endforeach; ?>
    </ul>
    <?php 
    // Reset the global post object so that the rest of the page works correctly.
    wp_reset_postdata(); ?>
<?php endif; ?>
<!-- ends related manufacturer link -->

<div class="clear">
            <?php if( get_field('review_inclusion_title', 'option')): ?>
            <h3><?php the_field('review_inclusion_title', 'option'); ?></h3>
            <?php endif; ?>
            <?php the_field('review_inclusion_content', 'option'); ?>
 </div><!-- ends clearing div for review inclusion --> 

        <?php the_tags( __( 'Tags: ', 'amazingribs_reworked' ), ' ', '' ); ?>
      <?php edit_post_link( __( 'Edit', 'amazingribs_reworked' ), '<span class="edit-link">', '</span>' ); ?>  

                            <?php
                  // If comments are open or we have at least one comment, load up the comment template
                  if ( comments_open() || '0' != get_comments_number() ) :
                    comments_template();
                  endif;
                ?>
  

    </div><!-- ends the post content container -->

<?php endwhile;
endif;
?>
    <nav class="pagination">
        <?php pagination_bar(); ?>
    </nav>
<!-- ALL OF THIS USED TO BE IN THE FOOTER -->
</div><!-- /.row -->


<section class="comments">
   <?php if ( is_active_sidebar( 'upper-footer' ) ) : ?>
          <?php dynamic_sidebar( 'upper-footer' ); ?> 
<?php endif; ?>
</section>

</div><!-- ends container -->


<!-- CONDITION FOR ALTERNATE SIDEBAR -->
<div class="widget-section widget-left" id="amazing-filter">
          <?php if ( get_field( 'alternate_sidebar_over_ride' ) ): ?>
                     <?php the_field('alternate_sidebar_over_ride'); ?>
          <?php else: // field_name returned false ?>
                  <?php if ( ! dynamic_sidebar( 'left-review' ) ) : ?>
                  <?php endif; // end sidebar widget area ?>
          <?php endif; // end of if field_name logic ?>
</div>

<!-- ENDS CONDITION FOR ALTERNATE SIDEBAR -->

  
<!-- https://webprosmeetup.org/wp_is_mobile/ -->
<?php if ( wp_is_mobile() ) : ?>
<!-- nothing here we are dropping the right sidebar for mobile devices -->
<?php else : ?>
    <?php if ( is_active_sidebar( 'right-review' ) ) : ?>
      <div class="widget-section widget-right">
        <?php dynamic_sidebar( 'right-review' ); ?>
      </div>
    <?php endif; ?>
<?php endif; ?>

</div><!-- row? -->
</div><!-- container? -->
<!-- NOW GET THE MODIFIED FOOTER -->
<?php get_footer(); ?>