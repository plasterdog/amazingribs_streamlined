<?php 
/**
 *
 * @package amazingribs_reworked
 */
get_header();?>

<?php
if ( have_posts() ) : while ( have_posts() ): the_post(); ?>

    <div id="post-<?php the_ID(); ?>" class="post-item clear">
       
      <?php if ( get_field( 'alternate_featured_image_over_ride' ) ): ?>
            <a href="<?php the_permalink(); ?>" rel="bookmark">
        <img style="width:150px;" src="<?php the_field('alternate_featured_image_over_ride'); ?>"/></a>
      <?php else: // field_name returned false ?>
      <?php if ( has_post_thumbnail() ) { ?>
        <a href="<?php the_permalink(); ?>" rel="bookmark">
        <?php the_post_thumbnail( 'thumbnail' ); ?></a>
      <?php } else { ?>
        <a href="<?php the_permalink(); ?>" rel="bookmark">
        <img style="width:150px;" src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/default-thumb.jpg" alt="<?php the_title(); ?>" /></a>
        <?php } ?>  
      <?php endif; // end of if field_name logic ?>


       <div class="content">

<!-- CHECKING FOR THE ALERNATE TITLE -->        
            <?php if ( get_field( 'alternate_title_over_ride' ) ): ?>
            <h2 class="archive-array"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_field('alternate_title_over_ride'); ?></a></h2>
            <?php else: // field_name returned false ?>
            <h2 class="archive-array"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
             <?php endif; // end of if field_name logic ?>

  <!-- AUTHOR ATTRIBUTION -->
  <?php if ( get_field( 'related_author' ) ): ?>
  <p class="author-attribution intro">By:</p>
  <?php
  $related_author = get_field('related_author');
  if( $related_author ): ?>
  <?php foreach( $related_author as $post ): 
  // Setup this post for WP functions (variable must be named $post).
  setup_postdata($post); ?>
    <p class="author-attribution"> <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></p>
  <?php endforeach; ?>
  <?php 
  // Reset the global post object so that the rest of the page works correctly.
  wp_reset_postdata(); ?>
<?php endif; ?>
<?php else: // field_name returned false ?>
 <!-- nothing right now-->

<?php endif; // end of if field_name logic ?>
<!-- ENDS AUTHOR ATTRIBUTION -->
       
<!-- ENDS TITLE & ATTRIBUTION SECTION -->

            <?php the_excerpt(); ?>
            <p style="text-align:right;"><a href="<?php the_permalink(); ?>" rel="bookmark">... read more</a></p>

        </div>
    </div>

<?php endwhile;
else:
?>
<h3>No results found.</h3>
<?php endif ?>

<?php do_action( 'custom_pagination' ); ?>
<!-- ALL OF THIS USED TO BE IN THE FOOTER -->
</div>
<!-- /.row -->
<section class="comments">
<?php if ( is_active_sidebar( 'upper-footer' ) ) : ?>
          <?php dynamic_sidebar( 'upper-footer' ); ?> 
<?php endif; ?>
</section>
</div>
<?php if ( is_active_sidebar( 'left-tool' ) ) : ?>
    <div class="widget-section widget-left">
       <?php dynamic_sidebar( 'left-tool' ); ?>
    </div>
<?php endif; ?>
<!-- /.container -->
</div>
<!-- https://webprosmeetup.org/wp_is_mobile/ -->
<?php if ( wp_is_mobile() ) : ?>
<!-- nothing here we are dropping the right sidebar for mobile devices -->
<?php else : ?>

<div class="widget-section widget-right">
    <?php if ( is_active_sidebar( 'right-tool' ) ) : ?>
      <?php dynamic_sidebar( 'right-tool' ); ?>
    <?php endif; ?>
<?php endif; ?>  
  
</div>
</div>
<!-- NOW GET THE MODIFIED FOOTER -->

<?php get_footer(); ?>