<?php 
/**
 *
 * @package amazingribs_reworked
 */
get_header();?>

    <div id="post-<?php the_ID(); ?>">

       <div class="post-excerpt">
  
<section class="error-404 not-found">
        <header class="page-header">
         <!-- THE SAME OPTIONS AS FOUND ON THE SEARCH PAGE FOR NO RESULT-->   
          <h1 class="page-title"><?php the_field('ar_404_title', 'option'); ?></h1>
        
        </header><!-- .page-header -->

        <?php the_field('ar_404_message_body', 'option'); ?>

          

<div class="clear" style="padding:2em 0; text-align:center;">
  <?php get_search_form(); ?>
</div>

      </section><!-- .error-404 -->



        </div><!-- ends post excerpt container div -->

    </div><!-- ends the post content container -->


    <nav class="pagination">
        <?php pagination_bar(); ?>
    </nav>
<!-- ALL OF THIS USED TO BE IN THE FOOTER -->
</div><!-- /.row -->


<section class="comments">
   <?php if ( is_active_sidebar( 'upper-footer' ) ) : ?>
          <?php dynamic_sidebar( 'upper-footer' ); ?> 
<?php endif; ?>
</section>

</div><!-- ends container -->


<!-- CONDITION FOR ALTERNATE SIDEBAR -->
<div class="widget-section widget-left">
          <?php if ( get_field( 'alternate_sidebar_over_ride' ) ): ?>
                     <?php the_field('alternate_sidebar_over_ride'); ?>
          <?php else: // field_name returned false ?>
                  <?php if ( ! dynamic_sidebar( 'left-side-bar' ) ) : ?>
                  <?php endif; // end sidebar widget area ?>
          <?php endif; // end of if field_name logic ?>
</div>

<!-- ENDS CONDITION FOR ALTERNATE SIDEBAR -->

<!-- https://webprosmeetup.org/wp_is_mobile/ -->
<?php if ( wp_is_mobile() ) : ?>
<!-- nothing here we are dropping the right sidebar for mobile devices -->
<?php else : ?>


  
<!-- https://webprosmeetup.org/wp_is_mobile/ -->
<?php if ( wp_is_mobile() ) : ?>
<!-- nothing here we are dropping the right sidebar for mobile devices -->
<?php else : ?>
    <?php if ( is_active_sidebar( 'right-side-bar' ) ) : ?>
      <div class="widget-section widget-right">
        <?php dynamic_sidebar( 'right-side-bar' ); ?>
      </div>
    <?php endif; ?>
<?php endif; ?>




<?php endif; ?>

</div><!-- row? -->
</div><!-- container? -->
<!-- NOW GET THE MODIFIED FOOTER -->
<?php get_footer(); ?>