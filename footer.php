<?php
/**
 * Footer template partial
 *
 * @package amazingribs_reworked
 *
 */
?>

<!-- /.center-body -->
<div class="mobile-ads"></div>
<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="clear">
                <?php if ( is_active_sidebar( 'footer1' ) ) : ?>
                   <div id="left-footer-widget"> <?php dynamic_sidebar( 'footer1' ); ?></div>
                <?php endif; ?>
           

                <?php if ( is_active_sidebar( 'footer2' ) ) : ?>
                 <div id="right-footer-widget">   <?php dynamic_sidebar( 'footer2' ); ?></div>
                <?php endif; ?>
            </div><!-- ends clear -->

            
            </div><!-- ends col-lg-8 col-md-10 mx-auto -->

        </div><!-- /.row -->
    </div><!-- /.container -->
</footer><!-- /footer -->

<div id="footer-copyright">
            &copy; <?php $the_year = date("Y"); echo $the_year; ?>  | All rights reserved
            <?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description' ); ?>
</div><!-- end footer copyright -->

<?php wp_footer(); ?>
</div>
<!-- /.wrapper -->
<script>
    jQuery(".search .fa-search").click(function() {
        var searchBox;
        searchBox =jQuery('.search .search-box');
        if(searchBox.hasClass( "open") == true) {
            searchBox.removeClass("open");
        } else {
            searchBox.addClass("open");
        }
    });
    var form = document.getElementById('searchForm');

    try {
        form.addEventListener("submit", submitFn, false);
    } catch(e) {
        form.attachEvent("onsubmit", submitFn); //IE8
    }
    function submitFn(event) {
        event.preventDefault();
        var boxes = document.getElementsByClassName('tagq');
        var checked = [];
        for(var i=0; boxes[i]; ++i){
            if(boxes[i].checked){
                checked.push(boxes[i].value);
            }
            boxes[i].checked = false;
        }

        var checkedStr = checked.join(",");
        document.getElementById('tag_request').value = checkedStr;
        form.submit();

        return false;
    }

</script>
</body>
</html>